$(function(){
	$('pre.brush\\:').each(function(){
		var lang = ($(this).attr('class').split(/\s+/))[1];
		lang = lang.substring(0,lang.length-1);
		$(this).removeClass(lang+';');
		$(this).addClass((RegExp(/^((as3|actionscript3|bash|shell|cf|coldfusion|c-sharp|csharp|cpp|c|css|delphi|pas|pascal|diff|patch|erl|erlang|groovy|js|jscript|javascript|java|jfx|javafx|perl|pl|php|plain|text|ps|powershell|py|python|rails|ror|ruby|scala|sql|vb|vbnet|xml|xhtml|xslt|html|xhtml))$/i).test(lang)) ? lang.toLowerCase()+';' : 'text;');
		$('<div class="aiuscode">Code '+lang+' :</div>').insertBefore($(this)).append($(this));
	});
});
