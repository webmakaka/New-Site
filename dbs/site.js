var mongoose = require('mongoose'),
	connect  = require('./connect').site;
	
db = mongoose.createConnection(connect);

db.model('news' ,new mongoose.Schema({
	title   : String,
	ownerId : String,
	body    : String,
	date    : { type: Date, default: Date.now }
}));

db.model('about',new mongoose.Schema({
	contenu : String
}));

db.model('account', (new mongoose.Schema({
	pseudo          : { type : String },
	email           : { type : String , match: /^[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*@[a-z0-9]+([_|\.|-]{1}[a-z0-9]+)*[\.]{1}[a-z]{2,6}$/i },
	pass            : String ,
	prenom          : { type : String, default : '' },
	nom             : { type : String, default : '' },
	age             : { type : Number, min : 0 ,default : 18 },
	commentaire     : { type : String , default : '' },
	date            : { type : Date, default : Date.now },
	posts           : Array ,
	imProfil        : { type : String,  default :  "http://lorempixel.com/250/250/cats/4"},
	imCouv          : { type : String,  default :  "http://aucklandvetdentist.co.nz/media/2076/faq_cat_banner.jpg"},
	groupe          : { type : Number, default : 20},
	ville           : { type : Number, default : 0},
	champDescriptif : { type : String, default : "Nouveau Membre"}
})));

db.model('post', new mongoose.Schema({
	pseudo  : String,
	date    : { type : Date, default : Date.now },
	message : String,
	topic   : String,
	theme   : String,
	section : String
}));

db.model('topic', new mongoose.Schema({
	pseudoCreateur : String,
	name           : String,
	date           : { type : Date, default : Date.now },
	lastPost       : { type : Date, default : Date.now },
	isResolue      : { type : Boolean, default : false},
	posts          : Array,
	theme          : String,
	section        : String
}));

db.model('theme',new mongoose.Schema({
	name    : String,
	topics  : Array,
	section : String
}));

db.model('section',new mongoose.Schema({
	name   : String,
	groupe : Number,
	themes : Array
}));
	
module.exports = db;
