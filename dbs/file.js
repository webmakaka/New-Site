var mongoose = require('mongoose'),
	connect  = require('./connect').file;
	
db = mongoose.createConnection(connect);

db.model('categoryFile' , new mongoose.Schema({
	name   : String,
	files  : Array,
	groupe : Number
}));

db.model('file' , new mongoose.Schema({
	pseudoCreateur : String,
	name           : String,
	category       : String,
	extension      : String,
	date           : { type : Date, default : Date.now },
	tags           : [String],
	text           : String,
	height         : Number,
	file_name      : String
}));
	
module.exports = db;
