var express    = require('express'),
	mongoose   = require('../dbs').file,
	sha1       = require('sha1'),
	md5        = require('js-md5'),
	multer     = require('multer'),
	fs         = require('fs'),
	htmlEncode = require('../html_encode'),

	router = express.Router(),

	err404 = new Error('Not Found');
	err404.status = 404;

router.use(function(req, res, next) {
	if (req.session.account.groupe >= 20)
		next();
	else
		next(err404);
});

router.get('/search', function(req, res, next) {
	if (req.query.fileTag !== undefined){
		var tagQuery = req.query.fileTag.split(/\s*,\s*/);
		mongoose.model('file').find({tags: {$elemMatch: {$in: tagQuery}}}, function(err, files) {
			res.render('file_search', {files : files, session : req.session});
		});
	}
	else
		next(err404);
});

router.get('/listeFile',function(req, res, next) {
	mongoose.model('file').find(null ,function(err, files){
		res.render('file_list', { files: files , session : req.session});
	});
});

router.use('/add',function(req, res, next){
	if (req.session.account.groupe >= 30)
		next();
	else
		next(err404);
});

router.use('/add',multer({
	dest: 'uploads/',
	rename: function (fieldname, filename) {
		return Date.now() + filename.replace(/\W+/g, '-').toLowerCase()
	}
}));

/* GET add file. */
router.get('/add', function(req, res, next) {
	mongoose.model('categoryFile').find(null , function(err, categoriesFile){
		res.render('file_add', { session : req.session , categoriesFile : categoriesFile });
	});
});

/* POST add file. */
router.post('/add', function(req, res, next) {
	if (typeof req.files.file === 'object' && req.body.category != "") {
		var fileModel = mongoose.model('file');
		var file = new fileModel;

		file.pseudoCreateur = req.session.account.pseudo;
		file.category = req.body.category;
		file.name = req.files.file.originalname;
		file.extension = req.files.file.extension;
		file.file_name = req.files.file.name;
		file.height = req.files.file.size;
		if (req.body.tags != undefined)
			file.tags = req.body.tags.split(/\s*,\s*/);
		file.text = htmlEncode(req.body.text);
		file.save(function(err, file){
			mongoose.model('categoryFile').update({name : file.category },{$push : {files : file.id}},function(err, file){});
			res.redirect('/file/'+file.file_name);
		})
	}
});

router.use('/:idFile',function(req, res, next) {
	mongoose.model('file').findOne({file_name : req.params.idFile},function(err, file){
		if(file == null)
			next(err404);
		else {
			req.file = file;
			next()
		}
	});
});

router.get('/:idFile',function(req, res, next) {
	res.render('file_file', { file: req.file , session : req.session});
});

router.get('/:idFile/download',function(req, res, next) {
	res.download('uploads/'+req.params.idFile);
});

router.get('/:idFile/edit',function(req, res, next) {
	res.render('file_edit', { file: req.file , session : req.session});
});

router.post('/:idFile/edit',function(req, res, next) {
	var file = req.file;
	file.category = req.body.category;
	if (req.body.tags != undefined)
		file.tags = req.body.tags.split(/\s*,\s*/);
	file.text = htmlEncode(req.body.explication);
	file.save();
	res.redirect('/file/'+req.params.idFile);
});

router.get('/:idFile/suppr',function(req, res, next) {
	fs.unlink('uploads/'+req.params.idFile);
	mongoose.model('file').remove({file_name : req.params.idFile},function(err, file){
		res.redirect('/file/listeFile');
	});
});

module.exports = router;
