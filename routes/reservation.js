var express = require('express');
var router = express.Router();
var mongoose = require('../dbs').gestion;
var sha1 = require('sha1');
var md5 = require('js-md5');
var multer = require('multer');

router.use(function(req, res, next) {
	if (req.session.account.groupe >= 20)
		next();
	else
	{
		var err = new Error('Not Found');
		err.status	 = 404;
		next(err);
	}
});

/* GET reservation page*/
router.get('/',function(req, res, next){
	res.render('reservation',{session : req.session});
});

/* POST reservation Croc */
router.post('/',function(req, res, next){

	var commande = JSON.parse(req.body.commande);
		
	if(typeof commande === "object"){
		if(typeof commande.menu === "number"){
			var resCroc = new (mongoose.model('reservationCroc'));
			
			if (typeof commande.name === 'string')
				resCroc.acheteur = commande.name;
			else
				resCroc.acheteur = req.session.account.pseudo;
			
			resCroc.menu = commande.menu;
			if (resCroc.menu == 0) {
				if(typeof commande.croc1 === "object" && typeof commande.croc2 ===  "object"){
					if(typeof commande.croc1.nature === "boolean" && typeof commande.croc1.jambon === "boolean" && typeof commande.croc1.tomate === "boolean" 
					&& typeof commande.croc2.nature === "boolean" && typeof commande.croc2.jambon === "boolean" && typeof commande.croc2.tomate === "boolean"){
						
						resCroc.croc1.nature = commande.croc1.nature;
						resCroc.croc1.jambon = commande.croc1.jambon;
						resCroc.croc1.tomate = commande.croc1.tomate;
						resCroc.croc2.nature = commande.croc2.nature;
						resCroc.croc2.jambon = commande.croc2.jambon;
						resCroc.croc2.tomate = commande.croc2.tomate;
						resCroc.heure = commande.heure;
						resCroc.prix = 230;
						
						resCroc.save();
						
						res.send('ok');
					}
				}
			}
			else if (resCroc.menu == 1){
				if(typeof commande.nature === "boolean" && typeof commande.jambon === "boolean" && typeof commande.tomate === "boolean" ){
					
					resCroc.croc1.nature = commande.nature;
					resCroc.croc1.jambon = commande.jambon;
					resCroc.croc1.tomate = commande.tomate;
					resCroc.heure = commande.heure;
					resCroc.prix = 100;
					
					resCroc.save();
					
					res.send('ok');
				}
			}
			else {
				if(typeof commande.banane === "boolean" && typeof commande.poire === "boolean") {
					resCroc.croc1.tomate = commande.banane;
					resCroc.croc2.tomate = commande.poire;
					resCroc.heure = 59;
					resCroc.prix = 50;
					
					resCroc.save();
					
					res.send('ok');
				}
			}
		}
	}
	
	if (!res.headersSent){
		err = new Error('Precondition Failed');
		err.status = 412;
		next(err)
	}
});

/* POST reservation Cable*/
router.post('/cable',function(req, res, next){
	var commande = JSON.parse(req.body.commande);
	if (typeof commande.longeur === "number" && typeof commande.croise === "boolean" && commande.longeur > 0){
		resCable = new (mongoose.model('reservationCable'));
		if (typeof commande.name === 'string')
				resCable.acheteur = commande.name;
			else
				resCable.acheteur = req.session.account.pseudo;
		resCable.longeur = commande.longeur;
		resCable.croise = commande.croise;
		if (resCable.longeur <=2) 
			resCable.prix = 150;
		else
			resCable.prix = 80*resCable.longeur;
		resCable.save();
		res.send('ok');
	}
	if (!res.headersSent){
		err = new Error('Precondition Failed');
		err.status = 412;
		next(err)
	}
});

module.exports = router;
