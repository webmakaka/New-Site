var express = require('express');
var router = express.Router();
var mongoose = require('../dbs').site;
var mongooseFile = require('../dbs').file;
var fs = require('fs');
var sha1 = require('sha1');
var md5 = require('js-md5');

router.use(function(req, res, next) {
	if (req.session.account.groupe >= 80)
		next();
	else
	{
		var err = new Error('Not Found');
		err.status	 = 404;
		next(err);
	}
});

/* GET admin user list page. */
router.get('/user', function(req, res, next) {
	mongoose.model('account').find(null , function(err, accounts){
		res.render('admin_user', { err: '' , session : req.session, accounts : accounts});
	});
});


/* GET admin user option page. */
router.get('/user/:idUser', function(req, res, next) {
	mongoose.model('account').findOne({ 'pseudo' : req.params.idUser }, function (err, account){
		if (err) { throw err; }
		if (account==null) {
			next();
		}
		else {
				res.render('admin_user_option', { account: account , session : req.session });
		}
	});
});

/* POST admin user option page*/
router.post('/user/:idUser', function(req, res, next) {

	mongoose.model('account').findOne({ 'pseudo' : req.params.idUser}, function (err, account){
		if (err) { throw err; }
		if (account==null) 
			next();
		else if (req.body.pass1 != req.body.pass2) 
			next();
		else{
			if (req.body.pass1 != "")
				account.pass = sha1(req.body.pass1);
			account.nom = req.body.nom;
			account.prenom = req.body.prenom;
			account.age = req.body.age;
			account.ville = req.body.ville;
			account.commentaire = req.body.commentaire;
		    account.email = req.body.mail;
			account.imProfil = req.body.imProfil;
			account.imCouv = req.body.imCouv;
			account.groupe = req.body.groupe;
			account.save();
			res.redirect('/admin/user/'+account.pseudo);
		}



	});
});


/* GET admin theme page. */
router.get('/theme', function(req, res, next) {
	mongoose.model('section').find(null,function (err, sections){
		if(sections != null){
			mongoose.model('theme').find(null , function(err, themes){
				res.render('admin_theme', { sections : sections, themes: themes , session : req.session});
			});
		}
		else
			res.render('admin_theme', { sections : [], themes: [], session : req.session});
	});
});

/* POST admin theme page. */
router.post('/theme', function(req, res, next) {

	var sectionModel = mongoose.model('section');
	var themeModel = mongoose.model('theme');
	
	sectionModel.findOne({name : req.body.section}, function (err , section){
		if(section==null){			
			mongoose.connection.close();
			res.redirect('/admin/theme');
		}
		else {
			themeModel.findOne({name : req.body.name,section:section.id}, function (err , theme){
				if(theme==null){
					theme=new themeModel;//*
					
					theme.name=req.body.name;
					theme.section=section.name;
					
					theme.save(function (err, theme){
						
						section.themes.push(theme.id);
						
						sectionModel.update({name : req.body.section} , {$set :{themes : section.themes }} ,function (err) {
							if (err) { throw err; }
							res.redirect('/admin/theme');
						});
					});
				}
				else 
					res.redirect('/admin/theme');
			});
		}
	});
});

/* GET admin theme option page. */
router.get('/theme/:idSection/:idTheme', function(req, res, next) {
	mongoose.model('theme').findOne({name : req.params.idTheme, section : req.params.idSection }, function(err, theme){
		if(theme == null)
			next();
		else {
			mongoose.model('section').find(null,function (err, sections){
				res.render('admin_theme_option', { sections : sections, theme: theme , session : req.session});
			});
		}
	}); 

});

/* POST admin theme option page. */
router.post('/theme/:idSection/:idTheme', function(req, res, next) {
	var themeModel = mongoose.model('theme');
	var sectionModel = mongoose.model('section');
	var topicModel = mongoose.model('topic');
	themeModel.findOne({name : req.params.idTheme, section : req.params.idSection }, function(err, theme){
		if(theme == null)
			next();
		else {
			if(theme.name == req.body.nom && theme.section == req.body.section)
				res.redirect('/admin/theme/'+theme.section+'/'+theme.name);
			else {
				themeModel.findOne({name : req.body.nom, section : req.body.section }, function(err, theme2){//*
					if(theme2 == null){
						topicModel.find({'_id' : {$in : theme.topics}},function(err, topics){
							var idPosts = [];
							while (	topics.length > 0){
								idPosts = idPosts.concat(topics.pop().posts);
							}
							if(theme.name != req.body.nom){
								themeModel.update({ '_id' : theme.id},{ name : req.body.nom},function(){});
								topicModel.update({ '_id' : { $in : theme.topics}},{ theme : req.body.nom},function(){});
								mongoose.model('post').update({ '_id' : { $in : idPosts}},{ theme : req.body.nom},function(){});
								
							}
							if(theme.section != req.body.section){
								sectionModel.findOne({name : req.body.nom}, function(err, section2){
									if(section2 == null){
										res.redirect('/admin/theme/'+req.body.section+'/');
									}
									else {
										sectionModel.update({name: theme.section}, {$pull : { themes : theme.id}},function(){});
										sectionModel.update({name: req.body.section}, {$push : { themes : theme.id}},function(){});
										themeModel.update({ '_id' : theme.id},{ section : req.body.section},function(){});
										topicModel.update({ '_id' : { $in : theme.topics}},{ section : req.body.section},function(){});
										mongoose.model('post').update({ '_id' : { $in : idPosts}},{ section : req.body.section},function(){});
										res.redirect('/admin/theme/'+req.body.section+'/');
									}
								});
							}
							else 
								res.redirect('/admin/theme/'+theme.section+'/'+req.body.nom);						
						});
					}
					else
						next();
				});
			}
		}
	}); 
});

/* GET admin theme suppr. */
router.get('/theme/:idSection/:idTheme/suppr', function(req, res, next) {
	var themeModel = mongoose.model('theme');
	var sectionModel = mongoose.model('section');
	var topicModel = mongoose.model('topic');
	themeModel.findOne({name : req.params.idTheme, section : req.params.idSection }, function(err, theme){
		if(theme == null)
			res.redirect('/admin/theme/');
		else {
			topicModel.find({'_id' : {$in : theme.topics}},function(err, topics){
				var idPosts = [];
				while (	topics.length > 0){
					idPosts = idPosts.concat(topics.pop().posts);
				}
				sectionModel.update({name: theme.section}, {$pull : { themes : theme.id}},function(){});
				themeModel.remove({ '_id' : theme.id},function(){});
				topicModel.remove({ '_id' : { $in : theme.topics}},function(){});
				mongoose.model('post').remove({ '_id' : { $in : idPosts}},function(){});
				res.redirect('/admin/theme/');
			});
		}
		
	}); 
});

/* GET admin section page. */
router.get('/section', function(req, res, next) {
	mongoose.model('section').find(null , function(err, sections){
		res.render('admin_section', { sections: sections , session : req.session});
	});
});

/* POST admin section list page. */
router.post('/section', function(req, res, next) {

	var sectionModel = mongoose.model('section');
	
	sectionModel.findOne({name : req.body.name}, function (err , section){
		if(section==null){
			section=new sectionModel;
			
			section.name = req.body.name;
			section.groupe = req.body.groupe;
			
			section.save(function (err, section){
				res.redirect("/admin/section");
			});
		}
		else
			res.redirect("/admin/section");
	});
});

/* GET admin section option page. */
router.get('/section/:idSection', function(req, res, next) {
	mongoose.model('section').findOne({name : req.params.idSection}, function(err, section){
		if(section == null)
			next()
		else {
			mongoose.model('theme').find({'_id' : {$in : section.themes}},function(err , themes){
				res.render('admin_section_option', { section: section, themes : themes, session : req.session});
			});
		}
	});
});

/* POST admin section option page. */
router.post('/section/:idSection', function(req, res, next) {
	var groupe = parseInt(req.body.groupe);
	if (typeof req.body.name === 'string' && groupe !== NaN) {
		if (req.body.name.match( /^[a-zA-Z0-9-_]+$/) && groupe >= 0 ){
			mongoose.model('section').findOne({name : req.params.idSection}, function(err, section){
				if(section == null)
					next()
				else {
					section.groupe = groupe;
					if(section.name != req.body.name){
						section.name = req.body.name;
						mongoose.model('theme').find({'_id' : {$in : section.themes}},function(err, themes){
							mongoose.model('theme').update({'_id' : {$in : section.themes}},{section : section.name},function(){});
							var idTopics = [];
							while (	themes.length > 0){
								idTopics = idTopics.concat(themes.pop().topics);
							}
							mongoose.model('topic').find({'_id' : {$in : idTopics}},function(err, topics){
								mongoose.model('topic').update({'_id' : {$in : idTopics}},{section : section.name},function(){});
								var idPosts = [];
								while (	topics.length > 0){
									idPosts = idPosts.concat(topics.pop().posts);
								}
								mongoose.model('post').update({'_id' : {$in : idPosts}},{section : section.name},function(){});
								section.save()
								res.redirect('/admin/section/'+section.name);
							});
						});
					}
					else {
						section.save()
						res.redirect('/admin/section/'+section.name);
					}
				}
			});
		}
	}
});

/* GET admin section suppr */
router.get('/section/:idSection/suppr', function(req, res, next) {
	mongoose.model('section').findOne({name : req.params.idSection}, function(err, section){
		if(section != null){
			mongoose.model('theme').find({'_id' : {$in : section.themes}},function(err , themes){
				var idTopics = [];
				while (	themes.length > 0){
					idTopics = idTopics.concat(themes.pop().topics);
				}
				mongoose.model('topic').find({'_id' : {$in : idTopics}},function(err, topics){
					var idPosts = [];
					while (	topics.length > 0){
						idPosts = idPosts.concat(topics.pop().posts);
					}
					console.log('test');
					mongoose.model('section').remove({'_id' : section.id},function(){});
					mongoose.model('theme').remove({'_id' : {$in : section.themes}},function(){});
					mongoose.model('topic').remove({'_id' : {$in : idTopics}},function(){});
					mongoose.model('post').remove({'_id' : {$in : idPosts}},function(){});
				});
			});
		}
		res.redirect('/admin/section/');
	});
});


/* GET file category page. */
router.get('/file', function(req, res, next) {
	mongooseFile.model('categoryFile').find(null , function(err, categoriesFile){
		res.render('admin_file', { categoriesFile: categoriesFile , session : req.session});
	});
});

/* POST file category list page. */
router.post('/file', function(req, res, next) {
	var categoryFileModel = mongooseFile.model('categoryFile');
	
	categoryFileModel.findOne({name : req.body.name}, function (err , categoryFile){
		if(categoryFile==null){
			categoryFile=new categoryFileModel;
			
			categoryFile.name = req.body.name;
			categoryFile.groupe = req.body.groupe;
			
			categoryFile.save(function (err, categoryFile){
				res.redirect("/admin/file");
			});
		}
		else 
			res.redirect("/admin/file");
	});
});

/*router.use('/file/:idCategorieFile',function(req, res, next) {
	mongooseFile.model('categoryFile').findOne(
}*/

router.get('/file/:idCategorieFile/suppr', function(req, res, next) {
	var categoryFileModel = mongooseFile.model('categoryFile');
	var fileModel = mongooseFile.model('file')
	categoryFileModel.findOne({name : req.params.idCategorieFile}, function(err, categorieFile){
		if(categorieFile != null) {
			categoryFileModel.remove({ '_id' : categorieFile.id},function(){});
			fileModel.find({'_id' : { $in : categorieFile.files}}, function(err, files){
				for (file of files) {
					fileModel.remove({ '_id' : file.id},function(){});
					fs.unlink('uploads/'+file.file_name);
				}
			});
		}
		res.redirect('/admin/file/');
	}); 
});

module.exports = router;
