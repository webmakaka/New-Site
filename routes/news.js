var express  = require('express'),
	router   = express.Router(),
	mongoose = require('../dbs').site,
	htmlEncode = require('../html_encode');

var err404		= new Error('Not Found');
err404.status	= 404;

router.get('/add',function(req, res, next) {
	if (req.session.account.groupe >= 50)
		res.render('index_add', {session : req.session});
	else
		next(err404);
});

router.post('/add',function(req, res, next) {
	if (req.session.account.groupe >= 50){
		var newsModel = mongoose.model('news');
		newsModel.findOne({title : req.body.indexTitle},function(err, news){
			if(news == null){
				news = new newsModel;
				news.title = req.body.indexTitle;
				news.ownerId = req.session.account.pseudo;
				news.body = htmlEncode(req.body.indexContent);
				news.save();
				res.redirect('/news/'+req.body.indexTitle);
			}
			else
				next(err404);
		});
	}
	else
		next(err404);
});


router.get('/list',function(req, res, next) {
	mongoose.model('news').find(null ,function(err, news){
		res.render('index_list', { indexes: news , session : req.session});
	});
});

router.use('/:idNews', function(req, res, next) {
	mongoose.model('news').findOne({title : req.params.idNews},function(err, news){
		if(news == null)
			next(err404);
		else {
			req.news = news;
			next();
		}
	});
});

router.get('/:idNews',function(req, res, next) {
	res.render('index_index', { index: req.news , session : req.session});
});

router.get('/:idNews/edit',function(req, res, next) {
	if (req.session.account.groupe >= 50)
		res.render('index_edit', { index: req.news , session : req.session});
	else
		next(err404);
});

router.post('/:idNews/edit',function(req, res, next) {
	if (req.session.account.groupe >= 50) {
		req.news.title = req.body.indexTitle;
		req.news.body = htmlEncode(req.body.indexContent);
		req.news.save();
		res.redirect('/news/'+req.body.indexTitle);
	}
	else
		next(err404);
});

router.get('/:idNews/suppr',function(req, res, next) {
	if (req.session.account.groupe >= 50){
		mongoose.model('news').remove({title : req.params.idNews},function(err, news){
			res.redirect('/');
		});
	}
	else
		next(err404);
});

module.exports = router;
