# NEW SITE for AIUS

After our first site minitwr to the tool support as express , bootstrap and other framework, we want to address the programming of a site of an association. 

### Patch Note

0.0.1 : Beginning from the project minitwr

### Tech

AIUS website uses a number of open source projects to work properly:

* [Twitter Bootstrap] - For a beautiful website
* [node.js] - For make a web 2.0 you must to use it
* [Express] - For improve performance of node.js
* [mongodb] - It is essential for have a website nosql

### Installation

- You need Mongodb installed :

  + You can find it here :
  > http://www.mongodb.org/downloads?_ga=1.211305949.932219332.1430058358


- You need Node.js installed :
  + You can find it here :
  > https://nodejs.org/download/


After : 
```sh
$ git clone https://github.com/bsulyan/forum
$ cd forum
$ npm install
$ sudo -s
$ PORT=80 ./bin/www 
```

And access to http://localhost/

### Todo's

  https://docs.google.com/document/d/1KQTmdYe0wPQn_Mx7wsDu-pjiNoJqpbTBZPgcHdDECFE/edit?usp=sharing
