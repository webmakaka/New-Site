var express      = require('express'),
	path         = require('path'),
	favicon      = require('serve-favicon'),
	logger       = require('morgan'),
	cookieParser = require('cookie-parser'),
	bodyParser   = require('body-parser'),
	session      = require('express-session'),
	MongoStore   = require('connect-mongo')(session),
	mongoose     = require('./dbs'),
	sha1         = require('sha1'),
	fs           = require('fs'),

	routes      = require('./routes'),
	profil      = require('./routes/profil'),
	forum       = require('./routes/forum'),
	admin       = require('./routes/admin'),
	gestion     = require('./routes/gestion'),
	reservation = require('./routes/reservation'),
	file        = require('./routes/file'),
	wiki        = require('./routes/wiki'),
	
	app = express(),
	
	accessLogStream = fs.createWriteStream(__dirname + '/log/access.log', {flags: 'a'})

// view engine setup
app.set('views',[
	path.join(__dirname, 'views'),
	path.join(__dirname, 'views/about'),
	path.join(__dirname, 'views/admin'),
	path.join(__dirname, 'views/file'),
	path.join(__dirname, 'views/forum'),
	path.join(__dirname, 'views/gestion'),
	path.join(__dirname, 'views/news'),
	path.join(__dirname, 'views/wiki')
]);
app.set('view engine', 'jade');

app.use(favicon(__dirname + '/public/images/favicon.ico'));

app.use(logger('combined', {
	stream : accessLogStream,
	skip: function (req, res) { return res.statusCode < 400 }
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session( { secret : 'AuisForever',
		saveUninitialized : false,
		resave: false,
		store: new MongoStore({ url : require('./dbs/connect').site,
			autoRemove: 'disabled' //development
		})
	})
);

app.use(function (req , res ,next){
	if (req.session.isConnect === undefined){
		req.session.isConnect = false;
		req.session.account= {groupe : 0};
	}
	next();
});


app.use('/profil', profil);
app.use('/forum',forum);
app.use('/admin',admin);
app.use('/gestion',gestion);
app.use('/reservation',reservation);
app.use('/file',file);
app.use('/wiki',wiki);
app.use('/', routes);

mongoose.site.model('account').findOne({groupe : 80 }, function (err, account){
	if(account==null){
		var account = new (mongoose.site.model('account'));
		account.pseudo = 'admin';
		account.groupe = 80;
		account.pass = sha1('pass');
		account.save();
	}
	mongoose.wiki.model('page').findOne({title : 'index' }, function (err, page){
		if(page ==null){
			var page = new (mongoose.wiki.model('page'));
			page.title = 'index';
			page.ownerId = 'admin';
			page.body = '';
			page.tags = ['index'];
			page.save();
		}
		mongoose.site.model('about').findOne({}, function (err, page){
			if(page ==null){
				var about = new (mongoose.site.model('about'));
				about.contenu = 'Editer moi, éditer moi !!!';
				about.save()
			}
		});
	});
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status	 = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      session : req.session
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    session : req.session
  });
});

//app.listen(80);
module.exports = app;
